import React, { useState } from "react";
import EditIcon from "@material-ui/icons/Edit";
import DeleteIcon from "@material-ui/icons/DeleteForever";
import axios from "axios";
import { RouteComponentProps, Link, withRouter } from "react-router-dom";
import { setCostasUpdated, setCostaInFocus } from "../store/appStateActions";
import { connect } from "react-redux";
import { Costa } from "../../costaTypes";
import Button from "@material-ui/core/Button";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import DialogTitle from "@material-ui/core/DialogTitle";

interface Props extends RouteComponentProps<any> {
  costa: Costa;
}

interface DispatchProps {
  setCostasUpdated: typeof setCostasUpdated;
  setCostaInFocus: typeof setCostaInFocus;
}

const mapDispatchToProps: DispatchProps = {
  setCostasUpdated: setCostasUpdated,
  setCostaInFocus: setCostaInFocus
};

type AllProps = Props & DispatchProps;

const EditRemoveCosta = (props: AllProps) => {
  const [removeConfirmationOpen, setRemoveConfirmationOpen] = useState<boolean>(
    false
  );
  return (
    <div className="editRemoveCostaContainer">
      <Button
        variant="contained"
        color="primary"
        startIcon={<EditIcon />}
        onClick={() => {
          props.setCostaInFocus({
            _id: props.costa._id,
            name: props.costa.name,
            startDate: new Date(props.costa.startDate),
            endDate: new Date(props.costa.endDate),
            description: props.costa.description,
            organizer: props.costa.organizer,
            location: props.costa.location
          });
          props.history.push("/create");
        }}
      >
        Muokkaa
      </Button>
      <Button
        variant="contained"
        color="secondary"
        onClick={() => setRemoveConfirmationOpen(true)}
      >
        <DeleteIcon />
      </Button>

      <Dialog
        open={removeConfirmationOpen}
        onClose={() => setRemoveConfirmationOpen(false)}
      >
        <DialogTitle id="alert-dialog-title">
          Haluatko poistaa Costan?
        </DialogTitle>

        <DialogContent>
          <DialogContentText id="alert-dialog-description">
            Jos poistat Costan, sitä ei voi enää IKINÄ palautta!
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button
            onClick={() => {
              setRemoveConfirmationOpen(false);
              axios
                .delete(
                  "https://jyri-backend.herokuapp.com/costas/" + props.costa._id
                )
                .then(res => console.log(res.data));
              props.setCostasUpdated(true);
            }}
            color="secondary"
            variant="contained"
            autoFocus
          >
            Poista
          </Button>

          <Button
            onClick={() => setRemoveConfirmationOpen(false)}
            color="primary"
            variant="outlined"
          >
            Peruuta
          </Button>
        </DialogActions>
      </Dialog>
    </div>
  );
};

export default withRouter(connect(null, mapDispatchToProps)(EditRemoveCosta));
