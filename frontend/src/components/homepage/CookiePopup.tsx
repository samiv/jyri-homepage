import React, { useState, useEffect } from "react";
import { Button } from "@material-ui/core";
import "../styles/styles.scss";
import { connect } from "react-redux";
import { setPrivacyOpen } from "../store/appStateActions";
import { useCookies } from "react-cookie";

const USES_COOKIES_COOKIE_NAME = "uses_cookies";

interface DispatchProps {
  setPrivacyOpen: typeof setPrivacyOpen;
}

const mapDispatchToProps: DispatchProps = {
  setPrivacyOpen: setPrivacyOpen
};

const CookiePopup = (props: DispatchProps) => {
  const [popupOpen, setPopupOpen] = useState<boolean>(true);
  const [cookies, setCookie] = useCookies([USES_COOKIES_COOKIE_NAME]);

  useEffect(() => {
    if(cookies[USES_COOKIES_COOKIE_NAME] === 'true') {
      setPopupOpen(false);
    }
  }, [])

  return (
    <>
      {popupOpen && (
        <div className="CookiePopupContent">
          <div className="CookiePopupContentText">
            We use cookies to ensure the best experience on our website. If you
            use this webpage we assume that you accept our privacy policy.
          </div>
          <div className="CookiesPopupContentButtons">
            <div className="CookiesPopupContentButton">
              <Button
                color="primary"
                variant="contained"
                onClick={() => {
                  setPopupOpen(false);
                  setCookie(USES_COOKIES_COOKIE_NAME, "true", {
                    maxAge: 2592000
                  });
                }}
              >
                Accept
              </Button>
            </div>
            <div className="CookiesPopupContentButton">
              <Button
                color="primary"
                variant="contained"
                onClick={() => props.setPrivacyOpen(true)}
              >
                Read More
              </Button>
            </div>
          </div>
        </div>
      )}
    </>
  );
};

export default connect(null, mapDispatchToProps)(CookiePopup);
