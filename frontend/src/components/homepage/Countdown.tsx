import React, { useEffect, useState, useRef } from "react";
import { Costa } from "../../costaTypes";
import CountdownItem from "./CountdownItem";
import SadIcon from "@material-ui/icons/SentimentVeryDissatisfied";
import "../styles/styles.scss";
import { connect } from "react-redux";
import { ApplicationState } from "../../App";
import { CircularProgress } from "@material-ui/core";

interface Props {
  nextCosta: Costa | null;
}

interface PropsFromState {
  fetching: boolean;
  serverNotAvailable: boolean;
}

const mapStateToProps = ({ appState }: ApplicationState) => {
  return {
    fetching: appState.fetching,
    serverNotAvailable: appState.serverNotAvailable
  };
};

type AllProps = Props & PropsFromState;

interface CountdownObject {
  days: number;
  hours: number;
  min: number;
  sec: number;
}

const calculateCountdown = (date: Date): CountdownObject | null => {
  let diff: number =
    (Date.parse(new Date(date).toString()) -
      Date.parse(new Date().toString())) /
    1000;

  // clear countdown when date is reached
  if (diff <= 0) return null;

  const timeLeft: CountdownObject = {
    days: 0,
    hours: 0,
    min: 0,
    sec: 0
  };

  if (diff >= 86400) {
    timeLeft.days = Math.floor(diff / 86400);
    diff -= timeLeft.days * 86400;
  }

  if (diff >= 3600) {
    timeLeft.hours = Math.floor(diff / 3600);
    diff -= timeLeft.hours * 3600;
  }

  if (diff >= 60) {
    timeLeft.min = Math.floor(diff / 60);
    diff -= timeLeft.min * 60;
  }
  timeLeft.sec = Math.floor(diff);

  return timeLeft;
};

const Countdown = (props: AllProps) => {
  const [countdown, setCountdown] = useState<CountdownObject>({
    days: 0,
    hours: 0,
    min: 0,
    sec: 0
  });

  useEffect(() => {
    const interval = setInterval(() => {
      const date: CountdownObject | null = props.nextCosta
        ? calculateCountdown(props.nextCosta.startDate)
        : null;
      date ? setCountdown(date) : clearInterval(interval);
    }, 1000);
    return () => {
      clearInterval(interval);
    };
  }, [props]);

  return (
    <div
      className={
        props.nextCosta && new Date(props.nextCosta.startDate) < new Date()
          ? "CountdownContainer-ongoing"
          : "CountdownContainer"
      }
    >
      {props.fetching && (
        <div className="Progress">
          <div className="ProgressText">Loading...</div>
          <CircularProgress color="secondary" />
        </div>
      )}

      {!props.fetching && !props.serverNotAvailable && (
        <>
          <div className="CountdownItems">
            <CountdownItem count={countdown.days} text={"Päivää"} />
            <CountdownItem count={countdown.hours} text={"Tuntia"} />
            <CountdownItem count={countdown.min} text={"Minuuttia"} />
            <CountdownItem count={countdown.sec} text={"Sekuntia"} />
          </div>

          <div className="CountdownTitle">
            {props.nextCosta &&
              new Date(props.nextCosta.startDate) > new Date() &&
              "Aikaa " + props.nextCosta.name + "n alkuun:"}

            {!props.nextCosta && "Ei seuraavaa costaa "}
            {!props.nextCosta && <SadIcon fontSize="large" />}

            {props.nextCosta &&
              new Date(props.nextCosta.startDate) < new Date() &&
              props.nextCosta.name + " on käynnissä!"}
          </div>
        </>
      )}
      {props.serverNotAvailable && !props.fetching && (
        <div className="ServerNotAvailable">
          <div className="ServerNotAvailableText">
            Yhteyttä serveriin ei voitu muodostaa!
          </div>
        </div>
      )}
    </div>
  );
};

export default connect(mapStateToProps)(Countdown);
