import React from "react";
import { setPrivacyOpen } from "../store/appStateActions";
import "../styles/styles.scss";
import { connect } from "react-redux";

interface DispatchProps {
  setPrivacyOpen: typeof setPrivacyOpen;
}

const mapDispatchToProps: DispatchProps = {
  setPrivacyOpen: setPrivacyOpen
};

const Footer = (props: DispatchProps) => {
  return (
    <div className="FooterContent">
      <div
        className="FooterPrivacyText"
        onClick={() => props.setPrivacyOpen(true)}
      >
        Privacy Policy
      </div>
      <div className="FooterCopyrightText">
        &#169; 2020 Copyright Sami Valkonen | All Rights Reserved
      </div>
    </div>
  );
};

export default connect(null, mapDispatchToProps)(Footer);
