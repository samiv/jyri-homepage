import React from "react";
import "../styles/styles.scss";
import axios from "axios";
import { useEffect } from "react";
import { Costa } from "../../costaTypes";
import { withRouter } from "react-router-dom";
import Countdown from "./Countdown";
import { green, purple } from "@material-ui/core/colors";
import CostasContainer from "./CostasContainer";
import { ApplicationState } from "../../App";
import { connect } from "react-redux";
import {
  setCostas,
  setCostasUpdated,
  setFetching,
  setServerNotAvailable
} from "../store/appStateActions";

interface PropsFromState {
  costas: Costa[];
  costasUpdated: boolean;
}

const mapStateToProps = ({ appState }: ApplicationState) => {
  return {
    costas: appState.costas,
    costasUpdated: appState.costasUpdated
  };
};

interface DispatchProps {
  setCostas: typeof setCostas;
  setCostasUpdated: typeof setCostasUpdated;
  setFetching: typeof setFetching;
  setServerNotAvailable: typeof setServerNotAvailable;
}

const mapDispatchToProps: DispatchProps = {
  setCostas: setCostas,
  setCostasUpdated: setCostasUpdated,
  setFetching: setFetching,
  setServerNotAvailable: setServerNotAvailable
};

const sortAndFilterCostas = (costas: Array<Costa>) => {
  const now: Date = new Date();
  costas.sort((a: Costa, b: Costa) => {
    if (!a && !b) {
      return 0;
    } else if (!a && b) {
      return -1;
    } else if (a && !b) {
      return 1;
    } else {
      return a.startDate > b.startDate ? 1 : -1;
    }
  });
  return costas.filter(c => {
    return new Date(c.endDate) > now;
  });
};

const fetchData = async (props: AllProps) => {
  props.setFetching(true);
  await axios
    .get("https://jyri-backend.herokuapp.com/costas")
    .then(response => {
        removePastCostas(response.data);
        props.setCostas(sortAndFilterCostas(response.data));
        props.setFetching(false);
    })
    .catch(error => {
      props.setFetching(false);
      props.setServerNotAvailable(true);
    });

  props.setCostasUpdated(false);
};

const removePastCostas = (costas: Array<Costa>) => {
  costas.map(c => {
    if (new Date(c.endDate) < new Date()) {
      axios
        .delete("https://jyri-backend.herokuapp.com/costas/" + c._id)
        .then(res => console.log(res.data));
    }
  });
};

type AllProps = PropsFromState & DispatchProps;

const HomePage = (props: AllProps) => {
  useEffect(() => {
    if (props.costasUpdated) {
      fetchData(props);
    }
  });

  return (
    <>
      <Countdown nextCosta={props.costas.length > 0 ? props.costas[0] : null} />
      <CostasContainer />
    </>
  );
};
export default withRouter(
  connect(mapStateToProps, mapDispatchToProps)(HomePage)
);
