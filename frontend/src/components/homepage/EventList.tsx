import React from "react";
import { Costa } from "../../costaTypes";
import CostaRow from "./CostaRow";
import EditRemoveCosta from "./EditRemoveCosta";
import "../styles/styles.scss";
import { ApplicationState } from "../../App";
import { connect } from "react-redux";

interface Props {}

interface PropsFromState {
  costas: Costa[];
}

type AllProps = Props & PropsFromState;

const mapStateToProps = ({ appState }: ApplicationState) => {
  return {
    costas: appState.costas
  };
};

const EventList = (props: AllProps) => {
  return (
    <>
      {props.costas.map((c: Costa, i: number) => {
        return (
          <div className="EventListItems" key={i}>
            <CostaRow
              costa={c}
              onGoing={new Date(c.startDate) < new Date()}
            />
            <EditRemoveCosta costa={c} />
          </div>
        );
      })}
    </>
  );
};
export default connect(mapStateToProps)(EventList);
