import React from "react";
import { Costa } from "../../costaTypes";

interface Props {
  costa: Costa;
  onGoing: boolean;
}

const CostaRow = (props: Props) => {
  const endDate: Date = new Date(props.costa.endDate);
  const startDate: Date = new Date(props.costa.startDate);

  return (
    <>
      {startDate < new Date() && (
        <div className="costaRowOngoingText">Käynnissä!</div>
      )}
      <div
        className={
          startDate < new Date()
            ? "costaRowContainer-ongoing"
            : "costaRowContainer"
        }
      >
        <div className="costaName">{props.costa.name}</div>
        <div className="costaRowRow">
          <div className="costaRowRowText">Paikka:</div> {props.costa.location}
        </div>
        <div className="costaRowRow">
          <div className="costaRowRowText">Järjestäjä:</div>{" "}
          {props.costa.organizer}
        </div>
        <div className="costaRowRow">
          <div className="costaRowRowText">Alkaa:</div>{" "}
          {startDate.toLocaleString()}
        </div>
        <div className="costaRowRow">
          <div className="costaRowRowText">Loppuu:</div>{" "}
          {endDate.toLocaleString()}
        </div>
        <div className="costaDescriptionText">Kuvaus:</div>
        <div className="costaDescription">{props.costa.description}</div>
      </div>
    </>
  );
};
export default CostaRow;
