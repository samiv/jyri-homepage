import React, { useState } from "react";
import EventList from "./EventList";
import "../styles/styles.scss";
import {
  Button,
  withStyles,
  makeStyles,
  createStyles
} from "@material-ui/core";
import AddCircle from "@material-ui/icons/AddCircle";
import { useEffect } from "react";
import { Costa } from "../../costaTypes";
import { Link, withRouter, RouteComponentProps } from "react-router-dom";
import { green, purple } from "@material-ui/core/colors";
import { setCostaInFocus } from "../store/appStateActions";
import { connect } from "react-redux";
import { emptyCosta } from "../store/appStateReducer";
import { ApplicationState } from "../../App";

const ColorButton = withStyles(() => ({
  root: {
    color: "white",
    backgroundColor: green[500],
    "&:hover": {
      backgroundColor: green[700]
    }
  }
}))(Button);

interface Props extends RouteComponentProps<any> {}

interface PropsFromState {
  costas: Costa[];
}

const mapStateToProps = ({ appState }: ApplicationState) => {
  return {
    costas: appState.costas
  };
};

interface DispatchProps {
  setCostaInFocus: typeof setCostaInFocus;
}

const mapDispatchToProps: DispatchProps = {
  setCostaInFocus: setCostaInFocus
};

type AllProps = Props & DispatchProps & PropsFromState;

const useStyles = makeStyles(() =>
  createStyles({
    margin: {
      margin: 5
    }
  })
);

const CostasContainer = (props: AllProps) => {
  return (
    <div className="CostaListContainer">
      <div className="AddCostaButtonContainer">
        <ColorButton
          // className={classes.margin}
          variant="contained"
          color="primary"
          startIcon={<AddCircle />}
          onClick={() => {
            setCostaInFocus(emptyCosta());
            props.history.push("/create");
          }}
        >
          Luo Costa
        </ColorButton>
      </div>
      <EventList />
    </div>
  );
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(CostasContainer));
