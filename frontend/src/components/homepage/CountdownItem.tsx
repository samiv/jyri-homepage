import React from "react";

interface Props {
  count: number;
  text: string;
}

const CountdownItem = (props: Props) => {
  return (
    <div className="CountdownItem">
      <div className="CountdownCount">{props.count}</div>
      <div className="CountdownText">{props.text}</div>
    </div>
  );
};

export default CountdownItem;
