import React, { useState } from "react";
import "../styles/modifyStyles.scss";
import { Costa } from "../../costaTypes";
import axios from "axios";
import { withRouter, RouteComponentProps, Link } from "react-router-dom";
import {
  BaseTextArea,
  CssTextField,
  CssTextFieldInvalid
} from "../textfields/customTextfield";
import { BaseButton } from "../buttons/primaryButton";
import SaveIcon from "@material-ui/icons/Save";
import CancelIcon from "@material-ui/icons/Cancel";
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import {
  makeStyles,
  createStyles,
  Dialog,
  DialogTitle,
  DialogContent,
  DialogContentText,
  DialogActions,
  Button
} from "@material-ui/core";
import { ApplicationState } from "../../App";
import { connect } from "react-redux";
import {
  setCostas,
  setCostasUpdated,
  setCostaInFocus,
  setValidated
} from "../store/appStateActions";
import { emptyCosta } from "../store/appStateReducer";

interface Props extends RouteComponentProps<any> {
  setAddCostaModalOpen: any;
}

interface PropsFromState {
  costas: any[];
  costaInFocus: Costa;
  validated: boolean;
}

const mapStateToProps = ({ appState }: ApplicationState) => {
  return {
    costas: appState.costas,
    costaInFocus: appState.costaInFocus,
    validated: appState.validated
  };
};

interface DispatchProps {
  setCostas: typeof setCostas;
  setCostasUpdated: typeof setCostasUpdated;
  setCostaInFocus: typeof setCostaInFocus;
  setValidated: typeof setValidated;
}

const mapDispatchToProps: DispatchProps = {
  setCostas: setCostas,
  setCostasUpdated: setCostasUpdated,
  setCostaInFocus: setCostaInFocus,
  setValidated: setValidated
};

type AllProps = Props & PropsFromState & DispatchProps;

const useStyles = makeStyles(() =>
  createStyles({
    buttonMargin: {
      margin: "20px"
    },
    marginBig: {
      marginTop: "50px",
      marginBottom: "30px"
    }
  })
);

const isEmpty = (text: string): boolean => {
  return !text || text === "";
};

const goToHomePage = (props: AllProps) => {
  props.setValidated(false);
  props.history.push("/");
};

const CreateNewEvent = (props: AllProps) => {
  const classes = useStyles();

  const [validationOpen, setValidationOpen] = useState<boolean>(false);

  const [startDate, setStartDate] = useState<Date>(
    props.costaInFocus.startDate
  );
  const [endDate, setEndDate] = useState<Date>(props.costaInFocus.endDate);
  const [name, setName] = useState<string>(props.costaInFocus.name);
  const [organizer, setOrganizer] = useState<string>(
    props.costaInFocus.organizer
  );
  const [location, setLocation] = useState<string>(props.costaInFocus.location);
  const [description, setDescription] = useState<string>(
    props.costaInFocus.description ? props.costaInFocus.description : ""
  );

  return (
    <div className="AddNewCostaContainer">
      <div className="AddNewCostaContainerContent">
        <div className="AddCostaMainTitle">Lisää uusi costa:</div>

        {props.validated && isEmpty(name) ? (
          <CssTextFieldInvalid
            id="nameField"
            className={classes.buttonMargin}
            label="Nimi"
            variant="outlined"
            autoComplete="off"
            onChange={e => setName(e.target.value)}
            value={name}
          />
        ) : (
          <CssTextField
            id="nameField"
            className={classes.buttonMargin}
            label="Nimi"
            variant="outlined"
            autoComplete="off"
            onChange={e => setName(e.target.value)}
            value={name}
          />
        )}
        <CssTextField
          id="organizer"
          className={classes.buttonMargin}
          label="Järjestäjä(t)"
          variant="outlined"
          autoComplete="off"
          onChange={e => setOrganizer(e.target.value)}
          value={organizer}
        />
        <CssTextField
          id="location"
          className={classes.buttonMargin}
          label="Sijainti"
          variant="outlined"
          autoComplete="off"
          onChange={e => setLocation(e.target.value)}
          value={location}
        />

        <div className="timeRow">
          <div className="AddCostaSubTitle">Alkaa:</div>
          <DatePicker
            selected={startDate}
            onChange={(date: Date) => setStartDate(date)}
            showTimeSelect
            timeFormat="HH:mm"
            dateFormat="dd/MM/yyyy HH:mm"
          />
        </div>
        <div className="timeRow">
          <div className="AddCostaSubTitle">Loppuu:</div>
          <DatePicker
            selected={endDate}
            onChange={(date: Date) => setEndDate(date)}
            showTimeSelect
            timeFormat="HH:mm"
            dateFormat="dd/MM/yyyy HH:mm"
          />
        </div>
        <div className="AddCostaTextArea">
          <CssTextField
            className={classes.marginBig}
            id={"description"}
            label={"Kuvaus"}
            multiline
            rows="5"
            variant="outlined"
            fullWidth={true}
            value={description}
            onChange={e => setDescription(e.target.value)}
          />
        </div>

        <div className="buttonBar">
          <BaseButton
            icon={<SaveIcon />}
            text="Tallenna"
            id="saveCostaButton"
            clickFunc={() => {
              if (
                isEmpty(name) ||
                isEmpty(description) ||
                isEmpty(organizer) ||
                isEmpty(location)
              ) {
                props.setValidated(true);
              } else {
                // Details from fields
                const costa = {
                  name: name ? name : "",
                  startDate: startDate ? startDate : new Date(),
                  endDate: endDate ? endDate : new Date(),
                  description: description ? description : "",
                  organizer: organizer ? organizer : "",
                  location: location ? location : ""
                };

                if (props.costaInFocus._id != null) {
                  axios.post(
                    "https://jyri-backend.herokuapp.com/costas/update/" +
                      props.costaInFocus._id,
                    costa
                  );
                } else {
                  axios
                    .post(
                      "https://jyri-backend.herokuapp.com/costas/add",
                      costa
                    )
                    .then(res => {
                      console.log(res.data);
                    })
                    .catch(err => console.log("error"));
                }

                props.setCostaInFocus(emptyCosta());
                props.setCostasUpdated(true);
                goToHomePage(props);
              }
            }}
          />
          <BaseButton
            icon={<CancelIcon />}
            text="Peruuta"
            id="cancelCreateCosta"
            clickFunc={() => {
              props.setCostaInFocus(emptyCosta());
              goToHomePage(props);
            }}
          />
        </div>
      </div>

      <Dialog open={validationOpen} onClose={() => setValidationOpen(false)}>
        <DialogTitle id="alert-dialog-title">Täytä kaikki kentät</DialogTitle>

        <DialogContent>
          <DialogContentText id="alert-dialog-description">
            Tyhjät kentät:
            <br />
            <br />
            {isEmpty(name) && "Nimi"}
            {isEmpty(name) && <br />}
            {isEmpty(organizer) && "Järjestäjä"}
            {isEmpty(organizer) && <br />}
            {isEmpty(location) && "Sijainti"}
            {isEmpty(location) && <br />}
            {isEmpty(description) && "Kuvaus"}
            {isEmpty(description) && <br />}
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button
            onClick={() => setValidationOpen(false)}
            color="secondary"
            variant="contained"
            autoFocus
          >
            Sulje
          </Button>
        </DialogActions>
      </Dialog>
    </div>
  );
};
export default withRouter(
  connect(mapStateToProps, mapDispatchToProps)(CreateNewEvent)
);
