import React from "react";

const NoMatch = () => {
  return <div className="NoMatch">Page Not Found :(</div>;
};

export default NoMatch;
