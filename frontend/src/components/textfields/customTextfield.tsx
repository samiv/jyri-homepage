import {
  withStyles,
  TextField,
  makeStyles,
  createStyles
} from "@material-ui/core";
import React from "react";

export const CssTextField = withStyles({
  root: {
    "& label.Mui-focused": {
      color: "pink"
    },
    "& .MuiOutlinedInput-root": {
      "& fieldset": {
        borderColor: "black"
      },
      "&:hover fieldset": {
        borderColor: "grey"
      },
      "&.Mui-focused fieldset": {
        borderColor: "pink"
      }
    }
  }
})(TextField);

export const CssTextFieldInvalid = withStyles({
  root: {
    "& label": {
      color: "red"
    },
    "& label.Mui-focused": {
      color: "red"
    },
    "& .MuiOutlinedInput-root": {
      "& fieldset": {
        borderColor: "red"
      },
      "&:hover fieldset": {
        borderColor: "brown"
      },
      "&.Mui-focused fieldset": {
        borderColor: "red"
      }
    }
  }
})(TextField);

const useStyles = makeStyles(() =>
  createStyles({
    margin: {
      margin: "20px"
    },
    marginBig: {
      marginTop: "40px",
      marginBottom: "30px"
    },
    background: {
      backgroundColor: "grey"
    }
  })
);

interface TextAreaProps {
  name: string;
  id: string;
  onChange: () => void;
}

export const BaseTextArea = (props: TextAreaProps) => {
  const classes = useStyles();
  return (
    <CssTextField
      className={classes.marginBig}
      id={props.id}
      label={props.name}
      multiline
      rows="5"
      variant="outlined"
      fullWidth={true}
    />
  );
};
