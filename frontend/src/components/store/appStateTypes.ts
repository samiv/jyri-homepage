import { Costa } from "../../costaTypes";

export interface AppState {
    costas: Costa[],
    costasUpdated: boolean,
    costaInFocus: Costa,
    privacyOpen: boolean,
    fetching: boolean,
    serverNotAvailable: boolean,
    validated: boolean,
}