import { Reducer } from "redux";
import { AppState } from "./appStateTypes";
import {
  SET_COSTAS,
  COSTAS_UPDATED,
  SET_COSTA_IN_FOCUS,
  SET_PRIVACY_OPEN,
  SET_FETCHING,
  SET_SERVER_NOT_AVAILABLE,
  SET_VALIDATED
} from "./appStateActions";
import { Costa } from "../../costaTypes";

export const emptyCosta = (): Costa => {
  return {
    _id: null,
    name: "",
    startDate: new Date(),
    endDate: new Date(),
    description: "",
    organizer: "",
    location: ""
  };
};

const initialState: AppState = {
  costas: [],
  costasUpdated: true,
  costaInFocus: emptyCosta(),
  privacyOpen: false,
  fetching: false,
  serverNotAvailable: false,
  validated: false
};

export const appStateReducer: Reducer<AppState> = (
  state = initialState,
  action
) => {
  switch (action.type) {
    case SET_COSTAS:
      return {
        ...state,
        costas: action.payload
      };
    case COSTAS_UPDATED:
      return {
        ...state,
        costasUpdated: action.payload
      };
    case SET_COSTA_IN_FOCUS:
      return {
        ...state,
        costaInFocus: action.payload
      };
    case SET_PRIVACY_OPEN:
      return {
        ...state,
        privacyOpen: action.payload
      };
    case SET_FETCHING:
      return {
        ...state,
        fetching: action.payload
      };
    case SET_SERVER_NOT_AVAILABLE:
      return {
        ...state,
        serverNotAvailable: action.payload
      };
    case SET_VALIDATED:
      return {
        ...state,
        validated: action.payload
      };
    default:
      return state;
  }
};
