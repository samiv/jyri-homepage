import { action } from 'typesafe-actions';
import { Costa } from '../../costaTypes';

export const SET_COSTAS = 'SET_COSTAS';
export const COSTAS_UPDATED = "COSTAS_UPDATED"
export const SET_COSTA_IN_FOCUS = "SET_COSTA_IN_FOCUS";
export const SET_PRIVACY_OPEN = "SET_PRIVACY_OPEN";
export const SET_FETCHING = "SET_FETCHING";
export const SET_SERVER_NOT_AVAILABLE = "SET_SERVER_NOT_AVAILABLE";
export const SET_VALIDATED = "SET_VALIDATED";

export const setCostas = (costas: Costa[]) => action(SET_COSTAS, costas);
export const setCostasUpdated = (updated: boolean) => action(COSTAS_UPDATED, updated);
export const setCostaInFocus = (costaInFocus: Costa) => action(SET_COSTA_IN_FOCUS, costaInFocus);
export const setPrivacyOpen = (privacyOpen: boolean) => action(SET_PRIVACY_OPEN, privacyOpen);
export const setFetching = (fetching: boolean) => action(SET_FETCHING, fetching);
export const setServerNotAvailable = (serverNotAvailable: boolean) => action(SET_SERVER_NOT_AVAILABLE, serverNotAvailable);
export const setValidated = (validated: boolean) => action(SET_VALIDATED, validated);