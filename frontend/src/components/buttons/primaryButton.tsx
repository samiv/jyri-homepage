import {
  withStyles,
  TextField,
  makeStyles,
  createStyles,
  Button
} from "@material-ui/core";
import React from "react";

const useStyles = makeStyles(() =>
  createStyles({
    margin: {
      margin: "15px"
    },
    background: {
      backgroundColor: "grey"
    }
  })
);

interface Props {
  id: string;
  text: string;
  clickFunc: () => void;
  icon?: JSX.Element;
}

export const BaseButton = (props: Props) => {
  const classes = useStyles();

  return (
    <Button
      startIcon={props.icon ? props.icon : null}
      id={props.id}
      variant="contained"
      className={classes.margin}
      color="secondary"
      onClick={props.clickFunc}
    >
      {props.text}
    </Button>
  );
};
