import React from "react";
import "./styles/styles.scss";
import { RouteComponentProps, withRouter } from "react-router-dom";
import { connect } from "react-redux";
import { setValidated } from "./store/appStateActions";

interface PropsFromState extends RouteComponentProps<any> {}

interface DispatchProps {
  setValidated: typeof setValidated;
}

const mapDispatchToProps: DispatchProps = {
  setValidated: setValidated
};

type AllProps = PropsFromState & DispatchProps;

const TitleBar = (props: AllProps) => {
  return (
    <div className="titlebar">
      <p
        className="titlebarText"
        onClick={() => {
          props.history.push("/");
          setValidated(false);
        }}
      >
        Jyri
      </p>
    </div>
  );
};
export default withRouter(connect(null, mapDispatchToProps)(TitleBar));
