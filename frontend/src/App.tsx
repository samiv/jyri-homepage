import React from "react";
import "./App.scss";
import HomePage from "./components/homepage/HomePage";
import TitleBar from "./components/TitleBar";
import { BrowserRouter, Switch, Route } from "react-router-dom";
import CreateNewEvent from "./components/modify/CreateNewEvent";
import { createStore, combineReducers } from "redux";
import { AppState } from "./components/store/appStateTypes";
import { appStateReducer } from "./components/store/appStateReducer";
import { Provider } from "react-redux";
import CookiePopup from "./components/homepage/CookiePopup";
import { CookiesProvider } from "react-cookie";
import Footer from "./components/homepage/Footer";
import PrivacyPolicyModal from "./components/homepage/PrivacyPolicyModal";
import { composeWithDevTools } from "redux-devtools-extension";
import NoMatch from "./components/NoMatch";

export interface ApplicationState {
  appState: AppState;
}

const rootReducer = combineReducers({
  appState: appStateReducer
});

let store = createStore(rootReducer, composeWithDevTools());

const App = () => {
  return (
    <Provider store={store}>
      <CookiesProvider>
        <BrowserRouter>
          <div className="App">
            <TitleBar />
            <div className="appContent">
              <Switch>
                <Route path="/" component={HomePage} exact />
                <Route path="/create" component={CreateNewEvent} />
                <Route component={NoMatch} />
              </Switch>
            </div>
            <Footer />
            <CookiePopup />
            <PrivacyPolicyModal />
          </div>
        </BrowserRouter>
      </CookiesProvider>
    </Provider>
  );
};

export default App;
