import * as mon from 'mongoose';

export interface Costa {
    _id: mon.Schema.Types.ObjectId | null,
    name: string,
    startDate: Date,
    endDate: Date,
    description?: string,
    organizer: string,
    location: string
}